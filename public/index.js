const STICKERS_TOTAL = 682;
let isInAddMode = true;
window.onload = function () {
	const stickersCount = getMyStickerCountArray();
	const mainElement = document.getElementById('album');
	for (let i = 0; i < STICKERS_TOTAL; i++) {
		mainElement.appendChild(getNewSticker(stickersCount, i));
	}
	configModeToggleButton();
	configCompareButton();
	configCopyButton();
	configImportButton();
};

/**
 * Configures the onclick event of the mode toggle button.
 */
function configModeToggleButton() {
	const modeToggleButton = document.getElementById('mode-toggle-button');
	modeToggleButton.onclick = function (ev) {
		ev.preventDefault(); if (isInAddMode === true) {
			modeToggleButton.className = 'remove';
			modeToggleButton.innerHTML = '-';
		} else {
			modeToggleButton.className = 'add';
			modeToggleButton.innerHTML = '+';
		}
		isInAddMode = !isInAddMode;
	};
}

/**
 * Configures the onclick event of the compare button.
 */
function configCompareButton() {
	const compareButton = document.getElementById('compare-button');
	compareButton.onclick = function (ev) {
		ev.preventDefault();
		const code = window.prompt('Cole o código abaixo:');
		if (code) {
			let their = window.arrayTransformer.textToArray(code);
			let mine = getMyStickerCountArray();
			if (mine.length === their.length) {
				mine = getArrayAsObject(mine);
				their = getArrayAsObject(their);
				let mineChange = filterRepetitions(mine, their);
				let theirChange = filterRepetitions(their, mine);
				mineChange = mineChange.splice(0, theirChange.length);
				theirChange = theirChange.splice(0, mineChange.length);
				const divTrades = document.getElementById('div-trades');
				divTrades.innerText = `${JSON.stringify(mineChange)} => ${JSON.stringify(theirChange)}`;
			}
		}
	};

	/**
	 * Filter out all stickers both from an to have and those from doesn't have.
	 * @param {number[]} from 
	 * @param {number[]} to 
	 * @returns {number[]}
	 */
	function filterRepetitions(from, to) {
		return from.filter(function (vm) {
			return !to.some(function (vt) {
				return vt['id'] === vm['id'];
			}) && vm['value'] > 1;
		}).map(function (v) {
			return v['id'];
		});
	}

	/**
	 * Turns an array into a sorted object array with sticker id and count value.
	 * @param {number[]} arr 
	 * @returns {{id:number, value:number}}
	 */
	function getArrayAsObject(arr) {
		return arr.map(function (v, i) {
			return { id: i, value: v };
		}).filter(function (v) {
			return v['value'] > 0;
		}).sort(function (a, b) {
			return b['value'] - a['value'];
		});
	}
}

/**
 * Configures the onclick event of the import button.
 */
function configImportButton() {
	const importButton = document.getElementById('import-button');
	importButton.onclick = function (ev) {
		ev.preventDefault();
		const code = window.prompt('Cole o código abaixo:');
		if (code) {
			localStorage.setItem('album', code);
			location.reload();
		}
	};
}

/**
 * Configures the onclick event of the copy button.
 */
function configCopyButton() {
	const copyButton = document.getElementById('copy-button');
	copyButton.onclick = function (ev) {
		ev.preventDefault();
		const textArea = document.createElement('textarea');
		document.body.appendChild(textArea);
		textArea.style.position = 'fixed';
		textArea.textContent = localStorage.getItem('album');
		textArea.select();
		try {
			return document.execCommand('copy');
		} catch (ex) {
			return false;
		} finally {
			document.body.removeChild(textArea);
		}
	};
	return copyButton;
}

/**
 * Returns a Proxy to a number[] representing each sticker's count.
 * The proxy is responsible for updating localStorage's album string.
 * @returns {Proxy}
 */
function getMyStickerCountArray() {
	let stickersCount = localStorage.getItem('album');
	try {
		stickersCount = window.arrayTransformer.textToArray(stickersCount)
			.map(function (item) {
				return parseInt(item, 10);
			});
	} catch (error) {
		stickersCount = new Array(STICKERS_TOTAL).fill(0);
	}
	return new Proxy(stickersCount, {
		set (arr, index, value) {
			arr[index] = value;
			localStorage.setItem('album', window.arrayTransformer.arrayToText(arr));
			return true;
		}
	});
}

/**
 * Gets a HTMLElement refering to a new sticker.
 * @param {number} stickersCount
 * @param {number} stickerNumber
 * @returns {HTMLElement}
 */
function getNewSticker(stickersCount, stickerNumber) {
	const sticker = document.createElement('div');
	const stickerNumberAsString = stickerNumber.toString().padStart(getStickerTotalDigitCount(), '0');
	addStyleAndText(stickersCount[stickerNumber], stickerNumberAsString, sticker);
	sticker.onclick = function () {
		if (isInAddMode) {
			stickersCount[stickerNumber]++;
		} else {
			if (stickersCount[stickerNumber] > 0) {
				stickersCount[stickerNumber]--;
			}
		}
		addStyleAndText(stickersCount[stickerNumber], stickerNumberAsString, sticker);
		return false;
	};
	return sticker;
}

/**
 * Gets the digit count of STICKERS_TOTAL.
 * @returns {number}
 */
function getStickerTotalDigitCount() {
	return (Math.log10((STICKERS_TOTAL ^ (STICKERS_TOTAL >> 31)) - (STICKERS_TOTAL >> 31)) | 0) + 1;
}

/**
 * Adds style and text to stickerElement based on quantity.
 * @param {number} quantity
 * @param {string} stickerNumber
 * @param {HTMLElement} stickerElement
 */
function addStyleAndText(quantity, stickerNumber, stickerElement) {
	if (quantity === 1) {
		stickerElement.innerHTML = `${stickerNumber} (${quantity})`;
		stickerElement.className = 'figurinha preenchida';
	} else if (quantity > 1) {
		stickerElement.innerHTML = `${stickerNumber} (${quantity})`;
		stickerElement.className = 'figurinha sobrando';
	} else {
		stickerElement.innerHTML = `${stickerNumber}`;
		stickerElement.className = 'figurinha';
	}
}
