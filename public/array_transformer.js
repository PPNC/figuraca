/* globals  LZString:false */
window.arrayTransformer = (function () {
	/**
	 * Transforms a number array into a compressed string
	 * @param {number[]} array
	 * @returns {string}
	 */
	function arrayToText(array) {
		return LZString.compressToBase64(JSON.stringify(array));
	}
	/**
	 * Transforms a compressed string into a number array
	 * @param {string} text
	 * @returns {number[]}
	 */
	function textToArray(text) {
		return JSON.parse(LZString.decompressFromBase64(text));
	}
	return {
		arrayToText,
		textToArray
	};
})();
